package ar.com.crud;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import ar.com.BiblioApplication;
import ar.com.dto.TemaDTO;
import ar.com.service.TemaService;



@RunWith(SpringRunner.class)
@SpringBootTest(classes = BiblioApplication.class)
public class BiblioApplicationTests {

	
	@Autowired
	private TemaService temaService;
	
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void crearTema() {
		TemaDTO tema1 = new TemaDTO("Novela");
		TemaDTO tema2 = new TemaDTO("Relato");
		
		tema1 = this.temaService.create(tema1);
		tema2 = this.temaService.create(tema2);
		
		TemaDTO temaDto1 = this.temaService.findById(tema1.getId());
		Assert.assertNotNull(temaDto1.getId());
		Assert.assertNotNull(temaDto1.getNombre());
		
	}

	@Test
	public void crearLibro() {
		// buscar 2 autores
		// List<Autor> a = this.autorService.findByApellido("Cortaza")
		
		// buscar un genero
		// this.generoService.findById(3)
		
		// buscar un tema
		
		
//		Libro l = new Llibro();
//		l.setAutores(a);
//		
//		this.libroService.crear(l);
		
	}
	
}
