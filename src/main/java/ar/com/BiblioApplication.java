package ar.com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages= {
		"ar.com.dto",
		"ar.com.model",
		"ar.com.service",
		"ar.com.repository",
		"ar.com.controller"
})

public class BiblioApplication {

	public static void main(String[] args) {
		SpringApplication.run(BiblioApplication.class, args);
	}

}
