package ar.com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity(name="Autor")
@Table(name="autor")
@SQLDelete(sql = "UPDATE autor SET state = 'DELETED' WHERE id = ?")
@Where(clause = "state is null")
public class Autor {

	//Atributos
	@Id
	@Column (name = "id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="nativoDeBaseDeDatos")
	@GenericGenerator(name="nativoDeBaseDeDatos", strategy="native")
	private Long id;
	
	@Column(name="Apellido")
	private String apellido;
	
	@Column(name="Nombre")
	private String nombre;
	
	@Column(name="Nacionalidad")
	private String nacionalidad;
	
	@Column(name = "State")
	private String state;

	//Constructores
	public Autor() {
		super();
	}
	
	public Autor(String apellido, String nombre, String nacionalidad) {
		this();
		this.apellido = apellido;
		this.nombre = nombre;
		this.nacionalidad = nacionalidad;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Autor))
			return false;
		Autor other = (Autor) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}
	
}
