package ar.com.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * @author Carlos
 *
 */
@Entity(name="EstadoEjemplar")
@Table(name="EstadoEjemplar")
public class EstadoEjemplar {
	
	public static final Long DISPONIBLE = new Long(1);
	public static final Long PRESTADO = new Long(2);
	public static final Long BAJA = new Long(3);
	
	//Atributos
		@Id
		@GeneratedValue(strategy=GenerationType.AUTO, generator="nativoDeBaseDeDatos")
		@GenericGenerator(name="nativoDeBaseDeDatos", strategy="native")
		private long id;
		
		@Column(name="Descripcion")
		private String descripcion;
		
		//Constructores
		public EstadoEjemplar() {
			super();
		}
		
		public EstadoEjemplar(String descrip) {
			this();
			this.descripcion = descrip;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + (int) (id ^ (id >>> 32));
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!(obj instanceof EstadoEjemplar))
				return false;
			EstadoEjemplar other = (EstadoEjemplar) obj;
			if (id != other.id)
				return false;
			return true;
		}

		// Get && Sets
		public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}

		public String getDescripcion() {
			return descripcion;
		}

		public void setDescripcion(String descripcion) {
			this.descripcion = descripcion;
		}

		public static Long getDisponible() {
			return DISPONIBLE;
		}

		public static Long getPrestado() {
			return PRESTADO;
		}

		public static Long getBaja() {
			return BAJA;
		}
		
}
