package ar.com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

/**
 * @author Carlos
 *
 */

@Entity(name="Editorial")
@Table(name="editorial")
@SQLDelete(sql = "UPDATE editorial SET state = 'DELETED' WHERE id = ?")
@Where(clause = "state is null")
public class Editorial {

	//Atributos
	@Id
	@Column (name = "id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="nativoDeBaseDeDatos")
	@GenericGenerator(name="nativoDeBaseDeDatos", strategy="native")
	private Long id;
	
	@Column(name="Nombre")
	private String nombre;
	
	@Column(name = "State")
	private String state;
	
	//Constructor
	public Editorial() {
		super();
	}
	
	public Editorial(String editorial) {
		this();
		this.nombre = editorial;
	}
	
	//Get & Sets
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Editorial))
			return false;
		Editorial other = (Editorial) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String editorial) {
		this.nombre = editorial;
	}
	
}
