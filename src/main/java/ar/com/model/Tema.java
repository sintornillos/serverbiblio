package ar.com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity(name = "Tema")
@Table(name = "tema")
@SQLDelete(sql = "UPDATE tema SET state = 'DELETED' WHERE id = ?")
@Where(clause = "state is null")
public class Tema {
	
	//Atributos
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="nativoDeBaseDeDatos")
	@GenericGenerator(name="nativoDeBaseDeDatos", strategy="native")
	private long id;
	
	@Column(name="Nombre")
	private String nombre;
	
	//Constructores
	public Tema() {
		super();
	}
	
	@Column(name = "State")
	private String state;
	
	public Tema(String nombre) {
		this();
		this.nombre = nombre;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Tema))
			return false;
		Tema other = (Tema) obj;
		if (id != other.id)
			return false;
		return true;
	}



	//getters y Setters
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
}
