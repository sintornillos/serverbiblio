package ar.com.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * @author Carlos
 *
 */
/*  Datos del prestamo
BibliotecaId - SocioId - EjemplarId - 
FechaPrestamo - FechaDevolucion - EstadoPrestamo  
*/

@Entity(name = "Prestamo")
@Table(name = "prestamo")
public class Prestamo {

	//Atributos
	@Id
	@Column (name="id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="nativoDeBaseDeDatos")
	@GenericGenerator(name="nativoDeBaseDeDatos", strategy="native")
	private Long id;

//	@ManyToOne
//	@JoinColumn(name = "BibliotecaId", foreignKey=@ForeignKey(name="fk_idBiblioteca_fk_id_prestamo"))
//	private Biblioteca biblioteca;

	@Column (name = "BibliotecaId")
	private Long bibliotecaId;
	
	@ManyToOne
	@JoinColumn(name = "socioId", foreignKey=@ForeignKey(name="fk_idSocio_fk_id_prestamo"))
	private Socio socio;
	
	@ManyToOne
	@JoinColumn(name = "ejemplarId", foreignKey = @ForeignKey(name="fk_idEjemplar_fk_id_prestamo"))
	private Ejemplar ejemplar;
	
	@Column(name="fechaPrestamo")
	private LocalDate fechaPrestamo; 

	@Column(name="fechaDevolucion")
	private LocalDate fechaDevolucion; 
	
	@Column(name="fechaDevuelto")
	private LocalDate fechaDevuelto;
	
	@ManyToOne
	@JoinColumn(name = "estadoId", foreignKey=@ForeignKey(name="fk_id_idPrestamo_idEstado"))
	private EstadoPrestamo estado;
	
	//Constructores
	public Prestamo() {
		super();
	}
	
//	public Prestamo(Long bibliotecaId, Socio socioId, Ejemplar ejemplarId, LocalDate fechaPrestamo, LocalDate fecDevolucion ) {
//		this.bibliotecaId = bibliotecaId;
//		this.socio = socioId;
//		this.ejemplar = ejemplarId;
//		this.fechaPrestamo = fechaPrestamo;
//		this.fechaDevolucion = fecDevolucion;
//	}	
	
	public Prestamo(Long biblioteca, Socio socioId, Ejemplar ejemplar, LocalDate fechaPrestamo, LocalDate fecDevolucion, EstadoPrestamo estado) {
		this.bibliotecaId = biblioteca;
		this.socio = socioId;
		this.ejemplar = ejemplar;
		this.fechaPrestamo = fechaPrestamo;
		this.fechaDevolucion = fecDevolucion;
		this.estado = estado;
	}

	public Prestamo(Long biblioteca, Socio socioId, Ejemplar ejemplar, LocalDate fechaPrestamo, LocalDate fecDevolucion, LocalDate fecDevuelto, EstadoPrestamo estado) {
		this.bibliotecaId = biblioteca;
		this.socio = socioId;
		this.ejemplar = ejemplar;
		this.fechaPrestamo = fechaPrestamo;
		this.fechaDevolucion = fecDevolucion;
		this.fechaDevuelto = fecDevuelto;
		this.estado = estado;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Prestamo))
			return false;
		Prestamo other = (Prestamo) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	//Gets y Sets
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getBiblioteca() {
		return bibliotecaId;
	}

	public void setBiblioteca(Long biblioteca) {
		this.bibliotecaId = biblioteca;
	}

	public Socio getSocio() {
		return socio;
	}

	public void setSocio(Socio socioId) {
		this.socio = socioId;
	}

	public Ejemplar getEjemplar() {
		return ejemplar;
	}

	public void setEjemplar(Ejemplar ejemplar) {
		this.ejemplar = ejemplar;
	}

	public LocalDate getFechaPrestamo() {
		return fechaPrestamo;
	}

	public void setFechaPrestamo(LocalDate fechaPrestamo) {
		this.fechaPrestamo = fechaPrestamo;
	}

	public LocalDate getFechaDevolucion() {
		return fechaDevolucion;
	}

	public void setFechaDevolucion(LocalDate fechaDevolucion) {
		this.fechaDevolucion = fechaDevolucion;
	}
	
	public LocalDate getFechaDevuelto() {
		return fechaDevuelto;
	}

	public void setFechaDevuelto(LocalDate fechaDevuelto) {
		this.fechaDevuelto = fechaDevuelto;
	}

	public EstadoPrestamo getEstado() {
		return estado;
	}

	public void setEstado(EstadoPrestamo estado) {
		this.estado = estado;
	}

	public Long getBibliotecaId() {
		return bibliotecaId;
	}

	public void setBibliotecaId(Long bibliotecaId) {
		this.bibliotecaId = bibliotecaId;
	}

}
