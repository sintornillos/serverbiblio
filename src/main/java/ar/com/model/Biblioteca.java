package ar.com.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


@Entity(name = "Biblioteca")
@Table(name = "biblioteca")
public class Biblioteca {

//	public static final Long BIBLIOTECA_ID = new Long(1);
	
	//Atributos
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="nativoDeBaseDeDatos")
	@GenericGenerator(name="nativoDeBaseDeDatos", strategy="native")
	private Long id;
	
	@Column(name="Nombre")
	private String nombre;
	
//	@OneToMany(mappedBy = "libro", cascade = CascadeType.ALL, orphanRemoval = true)
	@OneToMany
	@JoinColumn(name = "bibliotecaId")
	private List<Libro> libros;
	
	@OneToMany
	@JoinColumn(name = "bibliotecaId", foreignKey=@ForeignKey(name="fk_idSocio_id_biblioteca"))
	private List<Socio> socios;
	
	@OneToMany
	@JoinColumn(name = "bibliotecaId", foreignKey = @ForeignKey(name="fk_id_prestamo_id_biblioteca"))
	private List<Prestamo> prestamos;
	
	//Constructores
	public Biblioteca() {
		super();
	}
	
	public Biblioteca(String nombre) {
		this();
		this.nombre = nombre;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Biblioteca))
			return false;
		Biblioteca other = (Biblioteca) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	//getters y Setters
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
}
