/**
 * 
 */
package ar.com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ar.com.dto.EditorialDTO;
import ar.com.service.EditorialService;

/**
 * @author Carlos
 *
 */

@RestController
@RequestMapping("/editorial")
@CrossOrigin(origins = "*", maxAge = 3600, methods = {RequestMethod.POST, RequestMethod.GET, RequestMethod.DELETE, RequestMethod.PUT, RequestMethod.OPTIONS})
public class EditorialController {
		
	//Atributos
	@Autowired
	private EditorialService editorialService;
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<EditorialDTO> create(@RequestBody EditorialDTO editorialDTO){
		System.out.println("Nombre: " + editorialDTO.getNombre());
		editorialDTO = this.editorialService.create(editorialDTO);
		return new ResponseEntity<EditorialDTO>(editorialDTO, HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/findAll", method=RequestMethod.GET)
	public ResponseEntity<List<EditorialDTO>> findAll() throws NotFoundException{
		List<EditorialDTO> editorialDTO = this.editorialService.findAll();
		if(editorialDTO == null) {
			throw new NotFoundException();
		}
		return new ResponseEntity<List<EditorialDTO>>(editorialDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value="/findById/{id}", method=RequestMethod.GET)
	public ResponseEntity<EditorialDTO> findById(@PathVariable("id") long id) throws NotFoundException{
		EditorialDTO editorialDTO = this.editorialService.findById(id);
		if(editorialDTO == null) {
			throw new NotFoundException();
		}
		return new ResponseEntity<EditorialDTO>(editorialDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value="/delete/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable("id") long id) throws NotFoundException {
	    this.editorialService.deleteById(id);
		return new ResponseEntity<Void>(HttpStatus.ACCEPTED);
	}

	@RequestMapping(value="/findEditorial/{name}", method = RequestMethod.GET)
	public ResponseEntity<List<EditorialDTO>> findEditorial(@PathVariable("name") String nombre) throws NotFoundException{
		List<EditorialDTO> editorialDTO = this.editorialService.buscarEditorial(nombre);
		if(editorialDTO == null) {
			throw new NotFoundException();
		}
		return new ResponseEntity<List<EditorialDTO>>(editorialDTO, HttpStatus.OK);
	}	
	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity<Void> exceptionHandler(Exception excep){
		return new ResponseEntity<Void> (HttpStatus.NOT_FOUND);
	}
	
}
