package ar.com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ar.com.dto.SocioDTO;
import ar.com.service.SocioService;

@RestController
@RequestMapping("/socio")	
@CrossOrigin(origins = "*", maxAge = 3600, methods = {RequestMethod.POST, RequestMethod.GET, RequestMethod.DELETE, RequestMethod.PUT, RequestMethod.OPTIONS})
public class SocioController {

	// atributos
	@Autowired
	private SocioService socioService;

	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<SocioDTO> create(@RequestBody SocioDTO socioDTO){ 
		socioDTO = this.socioService.create(socioDTO);
		return new ResponseEntity<SocioDTO>(socioDTO, HttpStatus.CREATED);		
	}

	@RequestMapping(value="/findAll", method=RequestMethod.GET)
	public ResponseEntity<List<SocioDTO>> findAll() throws NotFoundException{
		List<SocioDTO> socioDto = this.socioService.findAll();
		if(socioDto == null) {
			throw new NotFoundException();
		}
		return new ResponseEntity<List<SocioDTO>>(socioDto, HttpStatus.OK);
	}
	
	@RequestMapping(value="/findById/{id}", method=RequestMethod.GET)
	public ResponseEntity<SocioDTO> findByID(@PathVariable("id") long id) throws NotFoundException{
		SocioDTO socioDto = this.socioService.findById(id);
		if(socioDto == null) {
			throw new NotFoundException();
		}
		return new ResponseEntity<SocioDTO>(socioDto, HttpStatus.OK);
	}
	
	@RequestMapping(value="/findByApe/{ape}", method=RequestMethod.GET)
	public ResponseEntity<List<SocioDTO>> findByApe(@PathVariable("ape") String apellido) throws NotFoundException{
		List<SocioDTO> socioDto = this.socioService.buscarApellido(apellido);
		if(socioDto == null) {
			throw new NotFoundException();
		}
		return new ResponseEntity<List<SocioDTO>>(socioDto, HttpStatus.OK);
	}
	
	@RequestMapping(value="/findByApeyNom", method=RequestMethod.GET)
	public ResponseEntity<List<SocioDTO>> findByApeyNom(@RequestParam(name="ape", required = false) String apellido, 
			@RequestParam(name="nom", required = false) String nombre, 
			@RequestParam(name="doc", required = false) Long nroDocumento) throws NotFoundException{

		List<SocioDTO> socioDto = this.socioService.buscarApeyNom(apellido, nombre, nroDocumento);
		if(socioDto == null) {
			throw new NotFoundException();
		}
		return new ResponseEntity<List<SocioDTO>>(socioDto, HttpStatus.OK);
	}
	
	@RequestMapping(value="/delete/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable("id") long id) throws NotFoundException {
	    this.socioService.deleteById(id);
		return new ResponseEntity<Void>(HttpStatus.ACCEPTED);
	}

	@RequestMapping(method=RequestMethod.PUT)
	public ResponseEntity<SocioDTO> update(@RequestBody SocioDTO socioDTO) throws NotFoundException{
		socioDTO = this.socioService.update(socioDTO);
		return new ResponseEntity<SocioDTO>(socioDTO, HttpStatus.OK);
	}
	
	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity<Void> exceptionHandler(Exception excep){
		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}
}