/**
 * 
 */
package ar.com.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ar.com.dto.LibroDTO;
import ar.com.service.LibroService;

/**
 * @author Carlos
 *
 */
@RestController
@RequestMapping("/libro")
@CrossOrigin(origins = "*", maxAge = 3600, methods = {RequestMethod.POST, RequestMethod.GET, RequestMethod.DELETE, RequestMethod.PUT, RequestMethod.OPTIONS})
public class LibroController {
	
	//Atributos
	@Autowired
	private LibroService libroService;
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<LibroDTO> create(@RequestBody LibroDTO libroDTO){
		libroDTO = this.libroService.create(libroDTO);
		return new ResponseEntity<LibroDTO>(libroDTO, HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/findAll", method=RequestMethod.GET)
	public ResponseEntity<List<LibroDTO>> findAll() throws NotFoundException{
		List<LibroDTO> libroDTO = this.libroService.findAll();
		if(libroDTO == null) {
			throw new NotFoundException();
		}
		return new ResponseEntity<List<LibroDTO>>(libroDTO,HttpStatus.OK);
	}

	@RequestMapping(value="/findById/{id}", method=RequestMethod.GET)
	public ResponseEntity<LibroDTO> findById(@PathVariable("id") long id) throws NotFoundException {
		LibroDTO libroDTO = this.libroService.findById(id);
		if(libroDTO == null) {
			throw new NotFoundException();
		}
		return new ResponseEntity<LibroDTO>(libroDTO,HttpStatus.OK);
	}
	
	@RequestMapping(value="/findBooks", method=RequestMethod.GET)
	public ResponseEntity<List<LibroDTO>> findBooks() throws NotFoundException{
		List<LibroDTO> libroDTO = this.libroService.findBooks();
		if(libroDTO == null) {
			throw new NotFoundException();
		}
		return new ResponseEntity<List<LibroDTO>>(libroDTO,HttpStatus.OK);
	}
	
	@RequestMapping(value="/findByTitulo/{titulo}", method=RequestMethod.GET)
	public ResponseEntity<List<LibroDTO>> findByTitulo(@PathVariable("titulo") String titulo) throws NotFoundException{
		List<LibroDTO> libroDTO = this.libroService.findByTitulo(titulo);
		if(libroDTO == null) {
			throw new NotFoundException();
		}
		return new ResponseEntity<List<LibroDTO>>(libroDTO,HttpStatus.OK);
	}
	
	@RequestMapping(value="/modify" , method=RequestMethod.PUT)
	public ResponseEntity<LibroDTO> update(@RequestBody LibroDTO libroDTO){
		libroDTO = this.libroService.update(libroDTO);
		return new ResponseEntity<LibroDTO>(libroDTO, HttpStatus.OK);
	}
	
	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity<Void> exceptionHandler(Exception excep){
		return new ResponseEntity<Void> (HttpStatus.NOT_FOUND);
	}
}
