package ar.com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ar.com.dto.AutorDTO;
import ar.com.service.AutorService;

@RestController
@RequestMapping("/autor")
@CrossOrigin(origins = "*", maxAge = 3600, methods = {RequestMethod.POST, RequestMethod.GET, RequestMethod.DELETE, RequestMethod.PUT, RequestMethod.OPTIONS})
public class AutorController {
	
	//Atributos
	@Autowired
	private AutorService autorService;
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<AutorDTO> create(@RequestBody AutorDTO autorDTO) {
		autorDTO = this.autorService.create(autorDTO);
		return new ResponseEntity<AutorDTO>(autorDTO, HttpStatus.CREATED);		
	}
	
	@RequestMapping(value="/findAll", method=RequestMethod.GET)
	public ResponseEntity<List<AutorDTO>> findAll() throws NotFoundException{
		List<AutorDTO> autorDTO = this.autorService.findAll();
		if(autorDTO == null) {
			throw new NotFoundException();
		}
		return new ResponseEntity<List<AutorDTO>>(autorDTO, HttpStatus.OK);
	}

	@RequestMapping(value="/findById/{id}", method=RequestMethod.GET)
	public ResponseEntity<AutorDTO> findById(@PathVariable("id") long id) throws NotFoundException {
		AutorDTO autorDTO = this.autorService.findById(id);
		if(autorDTO == null) {
			throw new NotFoundException();
		}
		return new ResponseEntity<AutorDTO>(autorDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value="/findByApe/{ape}", method=RequestMethod.GET)
	public ResponseEntity<List<AutorDTO>> finByApe(@PathVariable("ape") String apellido) throws NotFoundException{
		List<AutorDTO> autorDTO = this.autorService.buscarApellido(apellido);
		if(autorDTO == null) {
			throw new NotFoundException();
		}
		return new ResponseEntity<List<AutorDTO>>(autorDTO, HttpStatus.FOUND);
	}
	
	@RequestMapping(value="/findByApeyNom", method=RequestMethod.GET)
	public ResponseEntity<List<AutorDTO>> finByApeyNom(@RequestParam(name="ape", required=false) String apellido, 
					@RequestParam(name="nom", required=false) String nombre) throws NotFoundException{
		
		List<AutorDTO> autorDTO = this.autorService.buscarApeyNom(apellido, nombre);
		if(autorDTO == null) {
			throw new NotFoundException();
		}
		return new ResponseEntity<List<AutorDTO>>(autorDTO, HttpStatus.FOUND);
	}
		
	@RequestMapping(value="/delete/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable("id") long id) throws NotFoundException {
	    this.autorService.deleteById(id);
		return new ResponseEntity<Void>(HttpStatus.ACCEPTED);
	}
	
	@RequestMapping(value = "/modify", method = RequestMethod.PUT)
	public ResponseEntity<AutorDTO> update(@RequestBody AutorDTO autorDTO) throws NotFoundException {
		autorDTO = this.autorService.update(autorDTO);
		return new ResponseEntity<AutorDTO>(autorDTO, HttpStatus.OK);
	}
	
	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity<Void> exceptionHandler(Exception excep){
		return new ResponseEntity<Void> (HttpStatus.NOT_FOUND);
	}
		
}
