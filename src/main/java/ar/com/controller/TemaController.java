package ar.com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ar.com.dto.TemaDTO;
import ar.com.service.TemaService;

/**
 * @author Carlos 
 *
 */
@RestController
@RequestMapping("/tema")
@CrossOrigin(origins = "*", methods = {RequestMethod.POST, RequestMethod.GET, RequestMethod.DELETE, RequestMethod.PUT, RequestMethod.OPTIONS})
public class TemaController {
	
	//Atributos
	@Autowired
	private TemaService temaService;
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<TemaDTO> create(@RequestBody TemaDTO temaDTO) {
		temaDTO = this.temaService.create(temaDTO);
		return new ResponseEntity<TemaDTO>(temaDTO, HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/findAll", method=RequestMethod.GET)
	public ResponseEntity<List<TemaDTO>> findAll() throws NotFoundException{
		List<TemaDTO> temaDTO = this.temaService.findAll();
		if(temaDTO == null) {
			throw new NotFoundException();
		}
		return new ResponseEntity<List<TemaDTO>>(temaDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value="/findById/{id}", method=RequestMethod.GET)
	public ResponseEntity<TemaDTO> findById(@PathVariable("id") long id) throws NotFoundException {
		TemaDTO temaDTO = this.temaService.findById(id);
		if(temaDTO == null) {
			throw new NotFoundException();
		}
		return new ResponseEntity<TemaDTO> (temaDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value="/findByName/{name}", method=RequestMethod.GET)
	public ResponseEntity<List<TemaDTO>> findByName(@PathVariable("name") String name) throws NotFoundException {
		List<TemaDTO> temaDTO = this.temaService.buscarNombre(name);
		if(temaDTO == null) {
			throw new NotFoundException();
		}
		return new ResponseEntity<List<TemaDTO>>(temaDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value="/delete/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable("id") long id) throws NotFoundException {
	    this.temaService.deleteById(id);
		return new ResponseEntity<Void>(HttpStatus.ACCEPTED);
	}
	
	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity<Void> exceptionHandler(Exception excep) {
		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}
	
}
