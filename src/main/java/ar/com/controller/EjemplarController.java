package ar.com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ar.com.dto.EjemplarDTO;
import ar.com.service.EjemplarService;


/**
 * @author Carlos
 *
 */

@RestController
@RequestMapping("/ejemplar")
@CrossOrigin(origins = "*", maxAge = 3600, methods = {RequestMethod.POST, RequestMethod.GET, RequestMethod.DELETE, RequestMethod.PUT, RequestMethod.OPTIONS})
public class EjemplarController {
	
	//Atributos
	@Autowired
	private EjemplarService ejemplarService;
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<EjemplarDTO> create(@RequestBody EjemplarDTO ejemplarDTO){
		ejemplarDTO = this.ejemplarService.create(ejemplarDTO);
		return new ResponseEntity<EjemplarDTO>(ejemplarDTO, HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/findAll", method=RequestMethod.GET)
	public ResponseEntity<List<EjemplarDTO>> findAll() throws NotFoundException{
		List<EjemplarDTO> ejemplarDTO = this.ejemplarService.findAll();
		if(ejemplarDTO == null) {
			throw new NotFoundException();
		}
		return new ResponseEntity<List<EjemplarDTO>>(ejemplarDTO, HttpStatus.OK);
	}
		
	@RequestMapping(value="/findById/{id}", method=RequestMethod.GET)
	public ResponseEntity<EjemplarDTO> findById(@PathVariable("id") long id) throws NotFoundException{
		EjemplarDTO ejemplarDTO = this.ejemplarService.findById(id);
		if(ejemplarDTO == null) {
			throw new NotFoundException();
		}
		return new ResponseEntity<EjemplarDTO>(ejemplarDTO, HttpStatus.OK);
	}

	@RequestMapping(value="/findDisp/{idLibro}", method = RequestMethod.GET)
	public ResponseEntity<List<EjemplarDTO>> findDisp(@PathVariable("idLibro") Long idLibro) throws NotFoundException {
		List<EjemplarDTO> ejemplarDTO = this.ejemplarService.buscarDispon(idLibro);
		if(ejemplarDTO == null) {
			throw new NotFoundException();
		}
		return new ResponseEntity<List<EjemplarDTO>>(ejemplarDTO, HttpStatus.OK);
	}

	@RequestMapping(value="/modify", method=RequestMethod.PUT)
	public ResponseEntity<EjemplarDTO> update(@RequestBody EjemplarDTO ejemplarDTO) throws NotFoundException {
		ejemplarDTO = this.ejemplarService.update(ejemplarDTO);
		return new ResponseEntity<EjemplarDTO>(ejemplarDTO, HttpStatus.OK);
	}

	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity<Void> exceptionHandler(Exception excep){
		return new ResponseEntity<Void> (HttpStatus.NOT_FOUND);
	}
	
}
