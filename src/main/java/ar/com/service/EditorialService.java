/**
 * 
 */
package ar.com.service;

import java.util.List;

import ar.com.dto.EditorialDTO;

/**
 * @author Carlos
 *
 */
public interface EditorialService {

	public EditorialDTO create(EditorialDTO editorialDTO);
	
	public EditorialDTO findById(Long idEditorial);
	
	public List<EditorialDTO> findAll();

	public void deleteById(Long id);

	public EditorialDTO update(EditorialDTO editorialDTO);
	
	public List<EditorialDTO> buscarEditorial(String nombre);
	
}
