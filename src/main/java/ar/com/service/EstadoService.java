package ar.com.service;

import ar.com.dto.EstadoEjemplarDTO;

public interface EstadoService {
	
	public EstadoEjemplarDTO findById(long idEstado);

}
