/**
 * 
 */
package ar.com.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.com.dto.EjemplarDTO;
import ar.com.model.Editorial;
import ar.com.model.Ejemplar;
import ar.com.model.EstadoEjemplar;
import ar.com.model.Libro;
import ar.com.repository.EditorialRepository;
import ar.com.repository.EjemplarRepository;
import ar.com.repository.EjemplarEstadoRepository;
import ar.com.repository.LibroRepository;

/**
 * @author Carlos
 *
 */
@Service
public class EjemplarServiceImplem implements EjemplarService {
	
	//Atributos
	@Autowired
	private EjemplarRepository ejemplarRepository;
	
	@Autowired
	private EditorialRepository editorialRepository;
	
	@Autowired
	private EjemplarEstadoRepository ejemplarEstadoRepository;
	
	@Autowired
	private LibroRepository libroRepository;
	
	public EjemplarServiceImplem() {
		
	}
	
	//Metodos
	@Override
	@Transactional
	public EjemplarDTO create(EjemplarDTO ejemplarDTO) {
		Editorial editorial = this.editorialRepository.findById(ejemplarDTO.getEditorialId()).orElse(null);
		EstadoEjemplar estado = this.ejemplarEstadoRepository.findById(ejemplarDTO.getEstadoId()).orElse(null);
		Libro libro = libroRepository.findById(ejemplarDTO.getLibroId()).orElse(null);
		Ejemplar ejemplar = new Ejemplar(
				ejemplarDTO.getIsbn(),
				ejemplarDTO.getInventario(),
				ejemplarDTO.getSigTopografica(),
				ejemplarDTO.getLugar(),
				ejemplarDTO.getFecha(),
				editorial,
				estado,
				libro);
		this.ejemplarRepository.save(ejemplar);
		ejemplarDTO.setId(ejemplar.getId());
		return ejemplarDTO;
	}

	@Override 
	public EjemplarDTO findById(Long idEjemplar) {
		Ejemplar ejemplar = this.ejemplarRepository.findById(idEjemplar).orElse(null);
		EjemplarDTO ejemplarDTO = null;
		if (ejemplar != null) {
			ejemplarDTO = new EjemplarDTO(ejemplar);
		}
		return ejemplarDTO;
	}
	@Override
	public List<EjemplarDTO> findAll(){
		List<Ejemplar> ejemplares = (List<Ejemplar>) this.ejemplarRepository.findAll();
		List<EjemplarDTO> ejemplarDTO = new ArrayList<EjemplarDTO>();
		ejemplares.stream().forEach(ejem -> ejemplarDTO.add(new EjemplarDTO(ejem)));
		return ejemplarDTO;
	}

	@Override
	public List<EjemplarDTO> buscarDispon(Long idLibro) {
		List<Ejemplar> ejemplares = this.ejemplarRepository.buscarDisp(idLibro);
		List<EjemplarDTO> ejemplaresDTO = new ArrayList<EjemplarDTO>();
		ejemplares.stream().forEach(eje -> ejemplaresDTO.add(new EjemplarDTO(eje)));
		return ejemplaresDTO;
	}

	@Override
	public EjemplarDTO update(EjemplarDTO ejemplarDTO) {
		Editorial editorial = this.editorialRepository.findById(ejemplarDTO.getId()).orElse(null);
		EstadoEjemplar estado = this.ejemplarEstadoRepository.findById(ejemplarDTO.getId()).orElse(null);
		
		Ejemplar ejemplar = this.ejemplarRepository.findById(ejemplarDTO.getId()).orElse(null);
			ejemplar.setIsbn(ejemplarDTO.getIsbn());
			ejemplar.setInventario(ejemplarDTO.getInventario());
			ejemplar.setSigTopografica(ejemplarDTO.getSigTopografica());
			ejemplar.setLugar(ejemplarDTO.getLugar());
			ejemplar.setFecha(ejemplarDTO.getFecha());
			ejemplar.setEditorial(editorial);
			ejemplar.setEstado(estado);
			
			ejemplar = this.ejemplarRepository.save(ejemplar);
			
		return ejemplarDTO;
	}
}
