package ar.com.service;

import java.util.List;

import ar.com.dto.TemaDTO;

/**
 * @author Carlos 
 *
 */
public interface TemaService {
	
	public TemaDTO create(TemaDTO temaDTO);
	
	public TemaDTO findById(Long idTema);
	
	public List<TemaDTO> findAll();

	public void deleteById(Long id);

	public TemaDTO update(TemaDTO temaDTO);
	
	public List<TemaDTO> buscarNombre(String nombre);

}
