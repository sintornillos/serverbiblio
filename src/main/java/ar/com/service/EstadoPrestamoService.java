/**
 * 
 */
package ar.com.service;

import ar.com.dto.EstadoPrestamoDTO;

/**
 * @author Carlos
 *
 */
public interface EstadoPrestamoService {
	
	public EstadoPrestamoDTO findById(long idEstado);

}
