package ar.com.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.dto.PrestamoDTO;
import ar.com.model.Ejemplar;
import ar.com.model.EstadoEjemplar;
import ar.com.model.EstadoPrestamo;
import ar.com.model.Prestamo;
import ar.com.model.Socio;
import ar.com.repository.EjemplarEstadoRepository;
import ar.com.repository.EjemplarRepository;
import ar.com.repository.EstadoPrestamoRepository;
import ar.com.repository.PrestamoRepository;
import ar.com.repository.SocioRepository;

@Service
public class PrestamoServiceImplem implements PrestamoService {

	// Atributos
	
	@Autowired
	private PrestamoRepository prestamoRepository;
	
	@Autowired
	private EstadoPrestamoRepository estadoPrestamoRepository;

	@Autowired
	private SocioRepository socioRepository;
	
	@Autowired
	private EjemplarRepository ejemplarRepository;
	
	@Autowired
	private EjemplarEstadoRepository ejemplarEstadoRepository;
	
	public PrestamoServiceImplem() {
		
	}
	
	// Metodos
	
	@Override
	public PrestamoDTO create(PrestamoDTO prestamoDTO) {
		Socio socio = this.socioRepository.findById(prestamoDTO.getSocioId()).orElse(null);
		Ejemplar ejemplar = this.ejemplarRepository.findById(prestamoDTO.getEjemplarId()).orElse(null);
		EstadoPrestamo estadoPrest = this.estadoPrestamoRepository.findById(new Long(1)).orElse(null);
		LocalDate fecVieja = prestamoDTO.getFechaPrestamo();
		LocalDate fecDevolucion = fecVieja.plusDays(15);
		Prestamo prestamo = new Prestamo(
				prestamoDTO.getBibliotecaId(),
				socio,
				ejemplar,
				prestamoDTO.getFechaPrestamo(),
//				prestamoDTO.getFechaDevolucion(),
				fecDevolucion,
				estadoPrest
				);
			EstadoEjemplar estadoEjem = this.ejemplarEstadoRepository.findById(EstadoEjemplar.PRESTADO).orElse(null);
			ejemplar.setEstado(estadoEjem);
			this.prestamoRepository.save(prestamo);
			prestamoDTO.setId(prestamo.getId());
			return prestamoDTO;
	}

	@Override
	public PrestamoDTO findById(Long idPrestamo) {
		Prestamo prestamo = this.prestamoRepository.findById(idPrestamo).orElse(null);
		PrestamoDTO prestamoDTO = null;
		if(prestamo != null) {
			prestamoDTO = new PrestamoDTO(prestamo);
		}
		return prestamoDTO;
	}
	
	@Override
	public List <PrestamoDTO> findAll(){
		List<Prestamo> prestamos = (List<Prestamo>) this.prestamoRepository.findAll();
		List<PrestamoDTO> prestamosDTO = new ArrayList<PrestamoDTO>();
		prestamos.stream().forEach(prest -> {
			prestamosDTO.add(new PrestamoDTO(prest));	
		});
		
		return prestamosDTO;
	}
	
	@Override
	public PrestamoDTO devolver(PrestamoDTO prestamoDTO) {

		EstadoPrestamo estado = this.estadoPrestamoRepository.findById(EstadoPrestamo.DEVUELTO).orElse(null);
		
		Prestamo prestamo = this.prestamoRepository.findById(prestamoDTO.getId()).get();
		Ejemplar ejemplar = this.ejemplarRepository.findById(prestamo.getEjemplar().getId()).orElse(null);
			prestamo.setFechaDevuelto(LocalDate.now());

			prestamo.setEstado(estado);
			EstadoEjemplar estadoEjem = this.ejemplarEstadoRepository.findById(EstadoEjemplar.DISPONIBLE).orElse(null);
			ejemplar.setEstado(estadoEjem);
			this.prestamoRepository.save(prestamo);
		return prestamoDTO;
	}

//	@Override
//	public PrestamoDTO prestar(PrestamoDTO prestamoDTO) {
//		Ejemplar ejemplar = this.ejemplarRepository.findById(prestamoDTO.getId()).orElse(null);
//		EstadoPrestamo estado = this.estadoPrestamoRepository.findById(EstadoPrestamo.PRESTADO).orElse(null);
//		Prestamo prestamo = this.prestamoRepository.findById(prestamoDTO.getId()).get();
//			prestamo.setFechaDevolucion(prestamoDTO.getFechaDevolucion());
//
//			prestamo.setEstado(estado);
//			this.prestamoRepository.save(prestamo);
//			EstadoEjemplar estadoEjem = this.ejemplarEstadoRepository.findById(EstadoEjemplar.PRESTADO).orElse(null);
//			ejemplar.setEstado(estadoEjem);
//		return prestamoDTO;
//	}
	
	@Override
	public PrestamoDTO renovar(PrestamoDTO prestamoDTO) {		
//		Ejemplar ejemplar = this.ejemplarRepository.findById(prestamoDTO.getId()).orElse(null);

		EstadoPrestamo estado = this.estadoPrestamoRepository.findById(EstadoPrestamo.RENOVADO).orElse(null);
		Prestamo prestamo = this.prestamoRepository.findById(prestamoDTO.getId()).get();
			LocalDate fecVieja = prestamo.getFechaDevolucion();
//			prestamo.setFechaDevolucion(prestamoDTO.getFechaDevolucion());
			prestamo.setFechaDevolucion(fecVieja.plusDays(15));
			prestamo.setEstado(estado);
			
//			EstadoEjemplar estadoEjem = this.ejemplarEstadoRepository.findById(EstadoEjemplar.PRESTADO).orElse(null);
//			System.out.println("estado del ejemplar"+estadoEjem.getDescripcion());
//			ejemplar.setEstado(estadoEjem);
			
			this.prestamoRepository.save(prestamo);

		return prestamoDTO;
	}

}
