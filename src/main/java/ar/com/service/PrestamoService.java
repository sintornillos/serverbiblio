package ar.com.service;

import java.util.List;

import ar.com.dto.PrestamoDTO;

public interface PrestamoService {
	
	public PrestamoDTO create(PrestamoDTO prestamoDTO);

	public List<PrestamoDTO> findAll();

	public PrestamoDTO findById(Long id);

	public PrestamoDTO devolver(PrestamoDTO prestamoDTO);
	
//	public PrestamoDTO prestar(PrestamoDTO prestamoDTO);

	public PrestamoDTO renovar(PrestamoDTO prestamoDTO);
}
