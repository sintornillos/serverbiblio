/**
 * 
 */
package ar.com.service;

import java.util.List;

import ar.com.dto.LibroDTO;

/**
 * @author Carlos
 *
 */

public interface LibroService {
	
	public LibroDTO create(LibroDTO libroDTO);
	
	public LibroDTO findById(Long idLibro);
	
	public List<LibroDTO> findAll();

	public LibroDTO update(LibroDTO libroDTO);

	public List<LibroDTO> findBooks();

	public List<LibroDTO> findByTitulo(String titulo);
	
}
