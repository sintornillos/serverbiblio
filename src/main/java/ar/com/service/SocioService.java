/**
 * 
 */
package ar.com.service;

import java.util.List;

import ar.com.dto.SocioDTO;

/**
 * @author Carlos - Luz
 *
 */
public interface SocioService {

	public SocioDTO create(SocioDTO socioDTO);
	
	public SocioDTO findById(Long idSocio);

	public List<SocioDTO> findAll();

	void deleteById(Long idSocio);

	public SocioDTO update(SocioDTO socioDTO);

	public List<SocioDTO> buscarApellido(String ape);

	public List<SocioDTO> buscarApeyNom(String apellido, String nombre, Long nroDocumento);
	
}
