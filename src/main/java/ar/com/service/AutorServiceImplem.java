package ar.com.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.dto.AutorDTO;
import ar.com.model.Autor;
import ar.com.repository.AutorRepository;


@Service
public class AutorServiceImplem implements AutorService {

	//Atributos
	@Autowired
	private AutorRepository autorRepository;
	
	public AutorServiceImplem() {
		
	}
	
	//Metodos
	@Override
	public AutorDTO create(AutorDTO autorDTO) {
		Autor autor = new Autor(
				autorDTO.getApellido(),
				autorDTO.getNombre(),
				autorDTO.getNacionalidad()
				);
		this.autorRepository.save(autor);
		autorDTO.setId(autor.getId());
		return autorDTO;
	}

	@Override
	public AutorDTO findById(Long idAutor) {
		Autor autor = this.autorRepository.findById(idAutor).orElse(null);
		AutorDTO autorDTO = null;
		if(autor != null) {
			autorDTO = new AutorDTO(autor);
		}
		return autorDTO;
	}
	
	@Override
	public List<AutorDTO> buscarApellido(String apellido) {
		List<Autor>  autores = this.autorRepository.buscarApellido(apellido);
		List<AutorDTO> autoresDTO = new ArrayList<AutorDTO>();
		autores.stream().forEach(soc -> autoresDTO.add(new AutorDTO(soc)));
		return autoresDTO;
	}

	@Override
	public List<AutorDTO> buscarApeyNom(String apellido, String nombre) {
		List<Autor> autores = (List<Autor>) this.autorRepository.buscarApellidoNombre(apellido, nombre);
		List<AutorDTO> autoresDTO = new ArrayList<AutorDTO>();
		autores.stream().forEach(aut -> autoresDTO.add(new AutorDTO(aut)));
		return autoresDTO;
	}

	@Override
	public List<AutorDTO> findAll(){
		List<Autor> autores =(List<Autor>) this.autorRepository.findAll();
		List<AutorDTO> autorDTO = new ArrayList<AutorDTO>();
		autores.stream().forEach(aut -> autorDTO.add(new AutorDTO(aut)));
		return autorDTO;
	}

	@Override
	public void deleteById(Long idAutor) {
		this.autorRepository.deleteById(idAutor);
	}

	@Override
	public AutorDTO update(AutorDTO autorDTO) {
		Autor autor = this.autorRepository.findById(autorDTO.getId()).get();
			 autor.setApellido(autorDTO.getApellido());
			 autor.setNombre(autorDTO.getNombre());
			 autor.setNacionalidad(autorDTO.getNacionalidad());
			 autor = this.autorRepository.save(autor);
		return autorDTO;
	}

	
}
