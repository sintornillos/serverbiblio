/**
 * 
 */
package ar.com.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.com.dto.LibroDTO;
import ar.com.model.Autor;
import ar.com.model.Ejemplar;
import ar.com.model.Genero;
import ar.com.model.Libro;
import ar.com.model.Tema;
import ar.com.repository.AutorRepository;
import ar.com.repository.EjemplarRepository;
import ar.com.repository.GeneroRepository;
import ar.com.repository.LibroRepository;
import ar.com.repository.TemaRepository;

/**
 * @author Carlos
 *
 */

@Service
public class LibroServiceImplem implements LibroService {
	
	//Atributos
	@Autowired
	private LibroRepository libroRepository;
	
	@Autowired
	private TemaRepository temaRepository;
	
	@Autowired
	private GeneroRepository generoRepository;
	
	@Autowired
	private AutorRepository autorRepository;
	
	@Autowired
	private EjemplarRepository ejemplarRepository;
	
	public LibroServiceImplem() {
		
	}
	
	//Metodos
	@Override
	@Transactional
	public LibroDTO create(LibroDTO libroDTO) {
		Tema tema = this.temaRepository.findById(libroDTO.getTemaId()).orElse(null);
		Genero genero = this.generoRepository.findById(libroDTO.getGeneroId()).orElse(null);
				
		Libro libro = new Libro(
				libroDTO.getBibliotecaId(),
				libroDTO.getTitulo(),
				libroDTO.getSubTitulo(),
				libroDTO.getCdu(),
				tema,
				genero,
				libroDTO.getColeccion(),
				libroDTO.getDescripcion());
		
		libroDTO.getAutoresId().stream().forEach(autorId -> 
		 {
			 Autor autor = this.autorRepository.findById(autorId).orElse(null);
			 libro.addAutor(autor);
		 }
		);
		
		this.libroRepository.save(libro);
		libroDTO.setId(libro.getId());
		return libroDTO;
	}

	@Override
	public LibroDTO update(LibroDTO libroDTO) {
		Genero genero = this.generoRepository.findById(libroDTO.getGeneroId()).orElse(null);
		Tema tema = this.temaRepository.findById(libroDTO.getTemaId()).orElse(null);
		
		Libro libro = this.libroRepository.findById(libroDTO.getId()).get();
		libro.setTitulo(libroDTO.getTitulo());
		libro.setSubTitulo(libroDTO.getSubTitulo());
		
		libro.getAutores().clear();
		List<Autor> autoresNuevos = new ArrayList<Autor>();

		libroDTO.getAutoresId().stream().forEach(autorId -> { 
			Autor autor = this.autorRepository.findById(autorId).orElse(null);
			autoresNuevos.add(autor);
		});
		
		libro.setAutores(autoresNuevos);
		
		libro.setCdu(libroDTO.getCdu());
		libro.setGenero(genero);
		libro.setTema(tema);
		libro.setDescripcion(libroDTO.getDescripcion());
		libro.setColeccion(libroDTO.getColeccion());
		libro = this.libroRepository.save(libro);
		return libroDTO;
	}
	
	@Override
	public LibroDTO findById(Long idLibro) {
		Libro libro = this.libroRepository.findById(idLibro).orElse(null);
		LibroDTO libroDTO = null;
		if(libro != null) {
			libroDTO = new LibroDTO(libro);
		}
		return libroDTO;
	}

	@Override
	public List<LibroDTO> findAll() {
		List<Libro> libros = (List<Libro>) this.libroRepository.findAll();
		List<LibroDTO> libroDTO = new ArrayList<LibroDTO>();
		libros.stream().forEach(lib -> libroDTO.add(new LibroDTO(lib)));
		return libroDTO;
	}
	
	@Override
	public List<LibroDTO> findBooks(){
		List<Libro> books = (List<Libro>) this.libroRepository.findAll();
		List<LibroDTO> librosDTO = new ArrayList<LibroDTO>();
		books.stream().forEach(
				libro -> {
					LibroDTO libroDTO = new LibroDTO(libro);
					List<Ejemplar> ejemplares = this.ejemplarRepository.buscarDisp(libro.getId());
					libroDTO.setCtdadEjemplaresDisponibles(ejemplares.size());
					librosDTO.add(libroDTO);
				});
		return librosDTO;
 	}

	@Override
	public List<LibroDTO> findByTitulo(String titulo) {
//		Genero genero = this.generoRepository.findById(libroDTO.getGeneroId()).orElse(null);
//		Tema tema = this.temaRepository.findById(libroDTO.getTemaId()).orElse(null);
		
		List<Libro> libros = this.libroRepository.findByTitulo(titulo);
		List<LibroDTO> librosDTO = new ArrayList<LibroDTO>();
		libros.stream().forEach(libr -> librosDTO.add(new LibroDTO(libr)));
		return librosDTO;
//		libro.setTitulo(libroDTO.getTitulo());
//		libro.setSubTitulo(libroDTO.getSubTitulo());
//		
//		libro.getAutores().clear();
//		List<Autor> autoresNuevos = new ArrayList<Autor>();

//		libroDTO.getAutoresId().stream().forEach(autorId -> { 
//			Autor autor = this.autorRepository.findById(autorId).orElse(null);
//			autoresNuevos.add(autor);
//		});
		
//		libro.setAutores(autoresNuevos);
//		
//		libro.setCdu(libroDTO.getCdu());
//		libro.setGenero(genero);
//		libro.setTema(tema);
//		libro.setDescripcion(libroDTO.getDescripcion());
//		libro.setColeccion(libroDTO.getColeccion());
//		libro = this.libroRepository.save(libro);
//		return libroDTO;
	}


}
