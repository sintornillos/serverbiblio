package ar.com.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.dto.TemaDTO;
import ar.com.model.Tema;
import ar.com.repository.TemaRepository;



/**
 * @author Carlos
 *
 */

@Service
public class TemaServiceImplem implements TemaService {
	
	//Atributos
	@Autowired
	private TemaRepository temaRepository;
	
	public TemaServiceImplem() {
		
	}

	//Metodos
	@Override
	public TemaDTO create(TemaDTO temaDTO) {
		Tema tema = new Tema(
			 temaDTO.getNombre()
			 );
		this.temaRepository.save(tema);
		temaDTO.setId(tema.getId());
		return temaDTO;
	}
	
	@Override
	public TemaDTO findById(Long idTema) {
		Tema tema =  this.temaRepository.findById(idTema).orElse(null);
		TemaDTO temaDTO = null;
		if(tema != null) {
			temaDTO = new TemaDTO(tema);
		}	
		return temaDTO;
	}
	
	@Override
	public List<TemaDTO> buscarNombre(String nombre) {
		List<Tema> temas = (List<Tema>) this.temaRepository.buscarTema(nombre);
		List<TemaDTO> temasDTO = new ArrayList<TemaDTO>();
		temas.stream().forEach(name -> temasDTO.add(new TemaDTO(name)));
		return temasDTO;
	}
	
	@Override
	public List<TemaDTO> findAll() {
		List<Tema> temas = (List<Tema>) this.temaRepository.findAll();
		List<TemaDTO> temasDTO = new ArrayList<TemaDTO>();
		temas.stream().forEach(tem -> temasDTO.add(new TemaDTO(tem)));
		return temasDTO;
	}

	@Override
	public void deleteById(Long id) {
		this.temaRepository.deleteById(id);
	}
	
	@Override
	public TemaDTO update(TemaDTO temaDTO) {
		Tema tema = this.temaRepository.findById(temaDTO.getId()).get();
			 tema.getNombre();
			 this.temaRepository.save(tema);
			 return temaDTO;
	}
}
