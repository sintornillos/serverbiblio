package ar.com.repository;

import java.util.List;

import ar.com.model.Socio;

public interface SocioRepositoryCustomClass {

	public List<Socio> buscarApellidoNombre(String apellido, String nombre, Long dni);

}
