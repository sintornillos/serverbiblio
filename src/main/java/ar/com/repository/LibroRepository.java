/**
 * 
 */
package ar.com.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ar.com.model.Libro;


/**
 * @author Carlos
 *
 */
@Repository
public interface LibroRepository extends CrudRepository<Libro, Long>, LibroRepositoryCustom{


}
