package ar.com.repository;

import java.util.List;

import ar.com.model.Editorial;

public interface EditorialRepositoryCustomClass {
	
	public List<Editorial> buscarEditorial(String nombre);

}
