package ar.com.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import ar.com.model.Ejemplar;

public interface EjemplarRepositoryCustom {
	
	@Query("select e from Ejemplar e where libro_id=:idLibro and estado_id=1")
	public List<Ejemplar> buscarDisp(long idLibro);

}
