package ar.com.repository;

import org.springframework.data.repository.CrudRepository;

import ar.com.model.Tema;

public interface TemaRepository extends CrudRepository<Tema, Long>, TemaRepositoryCustom {

}
