/**
 * 
 */
package ar.com.repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ar.com.model.Ejemplar;

/**
 * @author Carlos
 *
 */
@Repository
public interface EjemplarRepository extends CrudRepository<Ejemplar, Long>, EjemplarRepositoryCustom {

	
}
