/**
 * 
 */
package ar.com.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import ar.com.model.Libro;

/**
 * @author Carlos
 *
 */
public interface LibroRepositoryCustom {
	
	@Query("select l from Libro l where l.titulo like %:titulo% ")
	public List<Libro> findByTitulo(String titulo);
	
}
