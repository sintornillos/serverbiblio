package ar.com.repository;

import java.util.ArrayList;
import java.util.List;


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.hibernate.Session;

import ar.com.model.Socio;

public class SocioRepositoryCustomClassImpl implements SocioRepositoryCustomClass {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	@Transactional
	public List<Socio> buscarApellidoNombre(String apellido, String nombre, Long dni) {
		Session session = this.entityManager.unwrap(Session.class);
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Socio> query = builder.createQuery(Socio.class);
		Root<Socio> root = query.from(Socio.class);
		
		List<Predicate> predicates = new ArrayList<Predicate>();
		if( apellido != null) {
			predicates.add(builder.like(root.<String>get("apellido"), "%" + apellido + "%"));			
		}
		if( nombre != null) {
			predicates.add(builder.like(root.<String>get("nombre"), "%" + nombre + "%"));			
		}
		if( dni != null) {
			predicates.add(builder.equal(root.<Long>get("nroDocumento"),  dni));			
		}
		
		query.where(builder.and(predicates.toArray(new Predicate[] {})));
		
		List<Socio> socios = session.createQuery(query).getResultList();
		return socios;
	}

}
