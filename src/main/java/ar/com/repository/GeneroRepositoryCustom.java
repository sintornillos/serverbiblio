package ar.com.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import ar.com.model.Genero;

public interface GeneroRepositoryCustom {
	
	@Query("SELECT g FROM Genero g WHERE g.nombre like %:nombre% ")
	public List<Genero> buscarNombre(String nombre);
	

}
