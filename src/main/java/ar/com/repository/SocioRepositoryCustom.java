package ar.com.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import ar.com.model.Socio;

public interface SocioRepositoryCustom {

	@Query("select s from Socio s where s.apellido like %:apellido% ")
	public List<Socio> buscarApellido(String apellido);

	
}
