package ar.com.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import ar.com.model.Editorial;

public interface EditorialRepositoryCustom {
	
	@Query("select e from Editorial e where e.nombre like %:nombre% ")
	public List<Editorial> buscarEditorial(String nombre);
	

}
