package ar.com.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.hibernate.Session;

import ar.com.model.Editorial;

public class EditorialRepositoryCustomClassImpl implements EditorialRepositoryCustomClass {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	@Transactional
	public List<Editorial> buscarEditorial(String nombre){
		Session session = this.entityManager.unwrap(Session.class);
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Editorial> query = builder.createQuery(Editorial.class);
		Root<Editorial> root = query.from(Editorial.class);
		
		List<Predicate> predicates = new ArrayList<Predicate>();
		if ( nombre != null) {
			predicates.add(builder.like(root.<String>get("nombre"), nombre));
			
		}
		query.where(builder.and(predicates.toArray(new Predicate[] {})));
		
		List<Editorial> editoriales = session.createQuery(query).getResultList();
		return editoriales;		
	}

}
