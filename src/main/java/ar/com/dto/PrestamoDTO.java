package ar.com.dto;

import java.time.LocalDate;

import ar.com.model.Prestamo;

/**
 * @author Carlos
 *
 */
public class PrestamoDTO {
	
	private Long id;
	private Long bibliotecaId;
	private Long socioId;
	private Long ejemplarId;
	private LocalDate fechaPrestamo;
	private LocalDate fechaDevolucion;
	private LocalDate fechaDevuelto;
	private Long estPrestamoId;
	private String nombre;
	private String apellido;
	private String inventario;
	private String titulo;
	
	
	//Constructores
	public PrestamoDTO() {
		super();
	}
	
	public PrestamoDTO(Prestamo prestamo) {
		this(prestamo.getBiblioteca(),
			 prestamo.getSocio().getId(),
			 prestamo.getEjemplar().getId(),
			 prestamo.getFechaPrestamo(),
			 prestamo.getFechaDevolucion(),
			 prestamo.getEstado().getId()
			 );
		this.id = prestamo.getId();
		this.apellido = prestamo.getSocio().getApellido();
		this.nombre = prestamo.getSocio().getNombre();
		this.inventario = prestamo.getEjemplar().getInventario();
		this.titulo = prestamo.getEjemplar().getLibro().getTitulo();
	}
	
	public PrestamoDTO(Long biblioId, Long socioId, Long ejemplarId, LocalDate fecPrestamo, LocalDate fecDevolucion) {
		this.bibliotecaId = biblioId;
		this.socioId = socioId;
		this.ejemplarId = ejemplarId;
		this.fechaPrestamo = fecPrestamo;
		this.fechaDevolucion = fecDevolucion;
	}
	
	public PrestamoDTO(Long biblioId, Long socioId, Long ejemplarId, LocalDate fecPrestamo, LocalDate fecDevolucion, Long estPrestamoId) {
		this.bibliotecaId = biblioId;
		this.socioId = socioId;
		this.ejemplarId = ejemplarId;
		this.fechaPrestamo = fecPrestamo;
		this.fechaDevolucion = fecDevolucion;
		this.estPrestamoId = estPrestamoId;
	}

	public PrestamoDTO(Long biblioId, Long socioId, Long ejemplarId, LocalDate fecPrestamo, LocalDate fecDevolucion, LocalDate fecDevuelto, Long estPrestamoId) {
		this.bibliotecaId = biblioId;
		this.socioId = socioId;
		this.ejemplarId = ejemplarId;
		this.fechaPrestamo = fecPrestamo;
		this.fechaDevolucion = fecDevolucion;
		this.fechaDevuelto = fecDevuelto;
		this.estPrestamoId = estPrestamoId;
	}
	// Gets & Sets

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getBibliotecaId() {
		return bibliotecaId;
	}

	public void setBibliotecaId(Long bibliotecaId) {
		this.bibliotecaId = bibliotecaId;
	}

	public Long getSocioId() {
		return socioId;
	}

	public void setSocioId(Long socioId) {
		this.socioId = socioId;
	}

	public Long getEjemplarId() {
		return ejemplarId;
	}

	public void setEjemplarId(Long ejemplarId) {
		this.ejemplarId = ejemplarId;
	}

	public LocalDate getFechaPrestamo() {
		return fechaPrestamo;
	}

	public void setFechaPrestamo(LocalDate fechaPrestamo) {
		this.fechaPrestamo = fechaPrestamo;
	}

	public LocalDate getFechaDevolucion() {
		return fechaDevolucion;
	}

	public void setFechaDevolucion(LocalDate fechaDevolucion) {
		this.fechaDevolucion = fechaDevolucion;
	}

	public LocalDate getFechaDevuelto() {
		return fechaDevuelto;
	}

	public void setFechaDevuelto(LocalDate fechaDevuelto) {
		this.fechaDevuelto = fechaDevuelto;
	}

	public Long getEstPrestamoId() {
		return estPrestamoId;
	}

	public void setEstPrestamoId(Long estPrestamoId) {
		this.estPrestamoId = estPrestamoId;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getInventario() {
		return inventario;
	}

	public void setInventario(String inventario) {
		this.inventario = inventario;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	
}
