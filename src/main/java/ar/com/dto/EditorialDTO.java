package ar.com.dto;

import ar.com.model.Editorial;

/**
 * @author Carlos
 *
 */
public class EditorialDTO {
	
	//Atributos
	private Long id;
	private String nombre;

	//Constructores
	public EditorialDTO() {
		super();
	}
	
	public EditorialDTO(String nombre) {
		super();
		this.nombre = nombre;
	}
	
	public EditorialDTO(Editorial editorial) {
		this(editorial.getNombre());
		this.id = editorial.getId();
	}

	//Gets & Sets
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
		
}
