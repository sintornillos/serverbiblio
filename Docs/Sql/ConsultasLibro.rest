// Extensión REST CLIENT 
// de Huachao Mao

@URL = http://localhost:8000

//-----------------Busco todos los libros 
###
 GET http://localhost:8000/libro/findAll

//-----------------Doy de alta un libro
###
POST http://localhost:8000/libro HTTP/1.1
Content-Type: application/json

{
	"bibliotecaId": "1",
	"titulo": "Martin Fierro",
	"subTitulo": "La vuelta",
	"autoresId": ["2"],
	"cdu": "AXN456",
	"generoId": "1",
	"temaId": "1",
	"coleccion": "MXN",
	"descripcion": "Libro trata el libro"
}

//-----------------Busco el libro con id = 1 
###
GET {{URL}}/libro/findById/2

//--
//-----------------Modifico el libro con id = 5 
###
PUT {{URL}}/libro/modify/ HTTP/1.1
Content-Type: application/json

{
  "id": 2,
  "bibliotecaId": 1,
  "titulo": "Martín Fierro",
  "subTitulo": "La vuelta ",
  "cdu": "AAA4444",
  "generoId": 3,
  "temaId": 1,
  "coleccion": "MDM",
  "descripcion": "Libro Gauchesco"
}

//-----------------Vuelvo a modificar el libro con id = 5 
###
PUT {{URL}}/libro/modify/ HTTP/1.1
Content-Type: application/json

{
  "id": 2,
  "bibliotecaId": 1,
  "titulo": "Martín Fierro",
  "subTitulo": "La vuelta ",
  "cdu": "AAA4444",
  "generoId": 3,
  "temaId": 1,
  "coleccion": "MDM",
  "descripcion": "Libro Gauchesco",
  "autoresId": ["2"]
}


//-----------------Busco el libro con id = 5
###
GET {{URL}}/libro/findByTitulo/Redes