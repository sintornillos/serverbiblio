// Extensión REST CLIENT 
// de Huachao Mao
// Este es una manera de probar las consultas mediante 
// un plugin de visual studio code llamado 
// REST CLIENT de Huachao Mao, se ejecuta cada consulta
// presionando control y haciendo click sobre la consulta

// Consultas a la tabla de temas
@URL = http://localhost:8000
###
GET http://localhost:8000/tema/findAll

###
POST http://localhost:8000/tema  HTTP/1.1
Content-Type: application/json

{
  "nombre": "Diccionario"
}

###
POST http://localhost:8000/tema  HTTP/1.1
Content-Type: application/json

{
  "nombre": "Literatura"
}
###
GET http://localhost:8000/tema/findById/3

###
{{URL}}/tema/findByName/cione