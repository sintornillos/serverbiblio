USE `dbdesa`;

LOCK TABLES `biblioteca` WRITE;
INSERT INTO `biblioteca` (`id`, `nombre`) VALUES (1,'Gral Paz');
UNLOCK TABLES;

-- 
-- --------    Tabla Autor  ---------------
-- 
LOCK TABLES `autor` WRITE;
INSERT INTO `autor` (`nombre`, `apellido`, `nacionalidad`, `state`) VALUES 
('Anonimo', 'Anonimo', '-', NULL),
("JOSÉ", "HERNANDEZ", "Arg", NULL),
('MAESTRO','DE ROSE','Bra', NULL),
('MICHAEL','CRICHTON','Usa', NULL),
('GUSTAV','FLAUBERT','Fra', NULL),
('MARCOS','PEREYRA','Arg', NULL),
('GIOCONDA','BELLI','Nic', NULL),
('MARY','HIGGINS CLARK','Usa', NULL),
('SOLEDAD','PEREYRA','Arg', NULL),
('MARIO JAVIER','VAENA','Arg', NULL),
('MARIA ELENA','WALSH','Arg', NULL),
('CRISTINA','LOZA','Arg', NULL),
('JAMES','DASHNER','Usa', NULL),
('FLORENCIA','BONELLI','Arg', NULL),
('RONALD','HUBBARD','Usa', NULL),
('JOHN','LE CARRE','Gbr', NULL),
('JOE','HILL','Usa', NULL);
UNLOCK TABLES;


-- 
-- --------    Tabla Editorial  ---------------
--
LOCK TABLES `editorial` WRITE;
INSERT INTO `editorial` (`nombre`, `state`) VALUES
('Otros', NULL),
('Santillana', NULL),
('Maipue', NULL),
('AZ', NULL),
('Bajo la luna', NULL),
('Colihue', NULL),
('Dakota', NULL),
('Panamericana', NULL),
('Malba', NULL),
('Continente', NULL);
UNLOCK TABLES;

--
-- ---------- `Tema` ---------------
--
LOCK TABLES tema WRITE;
INSERT INTO `tema` (`nombre`, `state`) VALUES 
('Otros', NULL),
('NOVELA', NULL),
('NARRATIVA JAPONESA', NULL),
('NARRATIVA CHILENA', NULL),
('NARRATIVA ESCOSESA', NULL),
('TEATRO ARGENTINO', NULL),
('LITERATURA JUVENIL', NULL),
('ILUSTRACIONES', NULL),
('ECONOMIA', NULL),
('INVESTIGACION PERIODISTICA', NULL),
('TEATRO INFANTIL', NULL),
('RELIGIONES DE LA INDIA', NULL),
('NARRATIVA ARGENTINA', NULL);
UNLOCK TABLES;

--
-- ----------- Tabla `socio` ---------------
--
LOCK TABLES `socio` WRITE;
INSERT INTO `socio` (`nro_docum`, `apellido`, `nombre`, `direcc`, `barrio`, `ciudad`, `telefono`, `email`, `celular`, `cpostal`, `fec_alta`, `nro_socio`, `sexo`, `categoria`, `biblioteca_id`) VALUES
('12121212', 'Miendez', 'Mirta', 'Calle 51 y 120', NULL, "Las Flores", '12423648', 'memirta@gmail.com', NULL, '1900', NULL, "A1", 'M', "8", 1),
('12121212', 'Lopez', 'Luis', 'Calle 21 y 1420', NULL, "Las Flores", '12423648', 'llopez@gmail.com', NULL, '1900', '2019-08-09', "A1", 'M', "8", 1),
('22112211', 'Baza', 'Marita', '31 nro 1102 depto 2', NULL, "Brandsen", '0221494946', 'marbaza@hotmail.com', NULL, '1900', NULL, "B1", 'M', "8", 1),
("343434348", "Fabrega", "Horacio", "5 nro 102 depto 5", NULL, "San Miguel", "0221494946", "mlaza@hotmail.com", NULL, 7100, NULL, "23", "M", "8", 1);
UNLOCK TABLES;

--
-- ------------ Tabla `estado_ejemplar`  ---------------------
--
LOCK TABLES `estado_ejemplar` WRITE;
INSERT INTO `estado_ejemplar` VALUES 
(1,'Disponible'),
(2,'Prestado');

UNLOCK TABLES;

--
-- ------------ Tabla `estado_prestamo`  ---------------------
--
LOCK TABLES `estado_prestamo` WRITE;
INSERT INTO `estado_prestamo` VALUES 
(1,'Nuevo'),
(2,'Renovado'),
(3,'Devuelto');
UNLOCK TABLES;
--
--  ----------------- Tabla `genero` --------------
--
LOCK TABLES `genero` WRITE;
INSERT INTO `genero` (`nombre`, `state`) VALUES 
('Otros', NULL),
('Policial', NULL),
('Relato', NULL),
('Novela', NULL);
UNLOCK TABLES;

-- LOCK TABLES `libro` WRITE;
-- INSERT INTO `libro` ( `biblioteca_id`, `cdu`, `coleccion`,
--   `descripcion`, `sub_titulo`, `titulo`, `genero_id`, `tema_id`) VALUES 
-- (1,'LMN456','MXN','Texto de que se trata el libro','Jugemos','Rayuela', 3, 1),
-- (1,'ABN456','MXN','Diseño de redes de largo alcance','Wan','Redes', 2, 7),
-- (1,'MRB876','Manuales','BD', 'En Visual Basic', 'Bases de Datos', 2, 7),
-- (1,'MRB877','Manuales','BD', 'En Visual Basic 6.0', 'Bases de Datos', 2, 7);
-- UNLOCK TABLES;

-- LOCK TABLES `ejemplar` WRITE;
-- INSERT INTO `ejemplar` ( `fecha`, `inventario`, `isbn`,
--   `lugar`, `sig_topog`, `editorial_id`, `estado_id`, `libro_id`) VALUES 
-- ('2019-10-15', 'AB123456', 123284, 'Italia', 'ABB211', 4, 1, 1),
-- ('2019-10-15', 'AB123457', 123285, 'Italia', 'ABB212', 4, 2, 1),
-- ('2019-09-11', 'BC123443', 123478, 'EEUU', 'ABA121', 7, 1, 2),
-- ('2019-09-11', 'BC123444', 123479, 'EEUU', 'ABA122', 7, 1, 2),
-- ('2019-09-08', 'BC123445', 123480, 'EEUU', 'ABB123', 7, 1, 2),
-- ('2019-08-08', 'DF223445', 125893, 'ARG', 'AAB231', 2, 1, 3),
-- ('2019-08-08', 'DF223446', 125894, 'ARG', 'AAB232', 2, 2, 3);
-- UNLOCK TABLES;