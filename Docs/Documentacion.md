## PRÓLOGO  

Este Trabajo Práctico (TP) fue hecho para atender los requerimientos de una Biblioteca en particular.  

Programación Orientada a Objetos. 
    Este TP fue diseñado siguiendo los lienamientos de la Programación Orientada a Objetos (POO)
    Decimos que estamos realizando POO cuando utilizamos clases, objetos y empleamos la herencia.  
    La herencia es un mecanismo para construir clases derivadas de otras, de forma que esta nueva clase tiene todas las características de la clase padre (atributos y métodos) y permite redefinirlas así como añadir otras nuevas. Con ello conseguimos, entre otras cosas, facilitar la construcción de TAD y permitir la reutilización de código.

JAVA
    El TP fue divido en dos partes, la parte de implementación de los servicios, almacenamiento o persistencia de datos, tambien llamada BACKEND, construida en JAVA, Maven, Spring Boot, y MySql.  

REACT
    Por otro lado, la parte donde interactúa el usuario, llamada FRONTEND, fue desarrollada en JAVASCRIPT con REACT, BOOSTRAP4  


### 1.1.CLASES DE OBJETOS  

Estas son las clases de objetos que hemos utilizados para realizar el TP
	Autor
    Biblioteca
    Editorial
	Ejemplar
	Genero
    Libro
	Prestamo
    Socio
	Tema  
	
### 2.1. DISEÑO DE LA BASE DE DATOS.  

    Según las especificaciones, debemos almacenar los siguientes datos:  
    Autores, Editoriales, Generos, Libros, Ejemplares, Prestamos, Reservas, Temas, Usuarios y Socios.  
    Por lo que las tablas que hemos necesitado para el TP son las siguientes:  
    Autor, Biblioteca, Editorial, Ejemplar, Genero, Libro, Prestamo, Socio, Tema y User  

### 2.1.1. Tabla XXX.  

    Támbien se creará una tabla XXX es en la que almacenaremos los niveles que existen para los usuarios, que en un principio son 2, “Administrador” y “Usuario”  

